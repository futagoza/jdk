[![npm version](https://img.shields.io/npm/v/jdk.svg)](https://www.npmjs.com/package/jdk) [![License](https://img.shields.io/badge/license-mit-blue.svg)](https://opensource.org/licenses/MIT)

JDK is my own personal JavaScript development kit. I use it to start, develop, test and manage most of my ECMAScript 6 based projects, mostly for Node.js 4+ and NW.js 0.13+

# install

###### from the command line tool

  ```shell
  npm install -g jdk
  ```

###### to use as a node.js module

  ```shell
  npm install --save-dev jdk
  ```

# license

Copyright (c) 2016+ Futago-za Ryuu ([gitlab](https://gitlab.com/u/futagoza), [github](https://github.com/futagoza))<br>
The MIT License, [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT)
