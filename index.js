'use strict';

var nodeVersion = process.version

if ( parseInt(nodeVersion.charAt(1)) < 6 ) {
  console.error('JDK requires Node.js 6+, you are use Node.js ' + nodeVersion)
  process.exit(1)
}
